import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'CourseProject';
  choixchargement = 'recette';

  onNavigate(choix: string){
      this.choixchargement = choix;
  }
}
