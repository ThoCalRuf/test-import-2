import { Component, OnInit } from '@angular/core';

import { Ingredient } from '../partager/ingredient.model';
@Component({
  selector: 'app-shopping-liste',
  templateUrl: './shopping-liste.component.html',
  styleUrls: ['./shopping-liste.component.css']
})
export class ShoppingListeComponent implements OnInit {
  ingredients: Ingredient[] =[
    new Ingredient('Pomme',5),
    new Ingredient('Tomate',10),
  ]; //Liste d'ingredient

  constructor() { }

  ngOnInit(): void {
  }

  IngredientAdded(ingredient : Ingredient){
    this.ingredients.push(ingredient);
  }

}
