import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';


import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { RecetteComponent } from './recette/recette.component';
import { RecetteListeComponent } from './recette/recette-liste/recette-liste.component';
import { RecetteDetailComponent } from './recette/recette-detail/recette-detail.component';
import { RecetteItemComponent } from './recette/recette-liste/recette-item/recette-item.component';
import { ShoppingListeComponent } from './shopping-liste/shopping-liste.component';
import { ShoppingEditComponent } from './shopping-liste/shopping-edit/shopping-edit.component';
import { DropdownDirective } from './partager/dropdown.directive';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    RecetteComponent,
    RecetteListeComponent,
    RecetteDetailComponent,
    RecetteItemComponent,
    ShoppingListeComponent,
    ShoppingEditComponent,
    DropdownDirective
  ],
  imports: [
    BrowserModule,
    FormsModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
