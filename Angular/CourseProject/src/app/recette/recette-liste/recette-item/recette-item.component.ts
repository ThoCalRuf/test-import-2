import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Recette } from '../../recette.model';

@Component({
  selector: 'app-recette-item',
  templateUrl: './recette-item.component.html',
  styleUrls: ['./recette-item.component.css']
})
export class RecetteItemComponent implements OnInit {
  @Input() recetteItem: Recette; 
  @Output() recetteDetail = new EventEmitter<void>(); //Recette que l'on click pour voir les details

  constructor() { }

  ngOnInit(): void {
  }

  onShowDetail(){
    this.recetteDetail.emit();
  }

}
