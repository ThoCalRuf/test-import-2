import { outputAst } from '@angular/compiler';
import { Component, EventEmitter, OnInit, Output } from '@angular/core';

import { Recette } from '../recette.model'

@Component({
  selector: 'app-recette-liste',
  templateUrl: './recette-liste.component.html',
  styleUrls: ['./recette-liste.component.css']
})
export class RecetteListeComponent implements OnInit {
  @Output() recetteSelectionner = new EventEmitter<Recette>();
  recettes: Recette[] = [
    new Recette('Test de recette','Juste un test','https://img.lemde.fr/2016/01/28/0/0/1000/667/664/0/75/0/ill_4855173_f14b_2016013045.0.1683467655yin1_ori.jpg'),
    new Recette('Autre de recette','Juste un autre test','https://img.lemde.fr/2016/01/28/0/0/1000/667/664/0/75/0/ill_4855173_f14b_2016013045.0.1683467655yin1_ori.jpg'),   
  ]; // Liste de recettes
  constructor() { }

  ngOnInit(): void {
  }

  recetteChoisie(recette : Recette){
    this.recetteSelectionner.emit(recette);
  }

}
