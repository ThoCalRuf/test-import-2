export class Recette{
    public name: string;//Nom de la recette
    public description: string;// Description de la recette
    public imagePath: string;//Image de la recette avec une Url

    constructor(name: string, description: string, imagePath: string){
        this.name = name;
        this.description = description;
        this.imagePath = imagePath;
    }
}