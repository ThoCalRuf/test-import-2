import { Component, OnInit } from '@angular/core';
import { Recette } from './recette.model';

@Component({
  selector: 'app-recette',
  templateUrl: './recette.component.html',
  styleUrls: ['./recette.component.css']
})
export class RecetteComponent implements OnInit {
  selectionRecette: Recette;

  constructor() { }

  ngOnInit(): void {
  }

}
