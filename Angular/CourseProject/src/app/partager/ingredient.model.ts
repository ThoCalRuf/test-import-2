export class Ingredient{
    public name: string; //Nom de l'ingredient
    public amount: number; //Montant d'ingredient

    constructor(name: string, amount: number){
        this.name = name;
        this.amount = amount;
    }
}