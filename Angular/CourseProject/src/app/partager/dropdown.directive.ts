import { Directive, HostBinding, HostListener } from "@angular/core";

@Directive({
    selector : '[appDropDown]'
})

export class DropdownDirective {
    @HostBinding('class.open') estOuvert = false; //Savoir si le menu dropdown est ouvert ou non

    //Change la valeur de estOuvert quand on click
    @HostListener('click') menuOuvert(eventDate : Event){
        this.estOuvert = !this.estOuvert;
    }
     
}