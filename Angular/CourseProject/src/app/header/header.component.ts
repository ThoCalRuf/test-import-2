import { Component, EventEmitter, Output } from '@angular/core';

@Component({
    selector: 'app-header',
    templateUrl: './header.component.html'
})
export class HeaderComponent {
    @Output() choixSelection =new EventEmitter<string>();

    onSelect(choix :string){
        this.choixSelection.emit(choix);
    }
}